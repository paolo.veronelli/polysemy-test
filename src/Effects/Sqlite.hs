{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Effects.Sqlite where

import Database.SQLite.Simple
import Polysemy
import Polysemy.Reader
import Polysemy.Resource
import Polysemy.State
import Protolude hiding (Reader, State, ask, bracket, modify, runReader)

data Sqlite (m :: * -> *) a where
  QueryS :: (ToRow q, FromRow r) => Query -> q -> Sqlite m [r]
  ExecuteS :: (ToRow q) => Query -> q -> Sqlite m ()

makeSem ''Sqlite

--- production -------------------------

runQueries
  :: Members '[Reader Connection, Embed IO] r
  => Sem (Sqlite : r) a
  -> Sem r a
runQueries = interpret \case
  QueryS q p -> do
    connection <- ask
    embed @IO $ query connection q p
  ExecuteS q p -> do
    connection <- ask
    embed @IO $ execute connection q p

withSqlite
  :: Members [Resource, Embed IO] r
  => String
  -> (Connection -> Sem r b)
  -> Sem r b
withSqlite h f = bracket
  do embed @IO $ open h
  do embed @IO . close
  do f

runSqlite
  :: Members '[Resource, Embed IO] r
  => String
  -> Sem (Sqlite : Reader Connection : r) b
  -> Sem r b
runSqlite h f = withSqlite h $ \c -> runReader c $ runQueries f

--- final exec

runSafe
  :: Sem '[Resource, Embed IO, Final IO] b
  -> IO b
runSafe = runFinal . embedToFinal @IO . resourceToIOFinal

--- testing ----------------------------------------------------

newtype SqliteQueryMockup = SqliteQueryMockup (forall q r. (Show q, Read r) => Query -> q -> [r])

newtype SqliteExecuteMockup = SqliteExecuteMockup (forall q. (Show q) => Query -> q -> ())

runTestQueries
  :: Members '[Reader SqliteQueryMockup, Reader SqliteExecuteMockup] r
  => Sem (Sqlite : r) a
  -> Sem r a
runTestQueries = interpret \case
  QueryS q p -> do
    SqliteQueryMockup query <- ask
    pure $ query q p
  ExecuteS q p -> do
    SqliteExecuteMockup query <- ask
    pure $ query q p

runTestSqlite
  :: Members '[] r
  => SqliteQueryMockup
  -> SqliteExecuteMockup
  -> Sem (Sqlite : Reader SqliteQueryMockup : Reader SqliteExecuteMockup : r) b
  -> Sem r b
runTestSqlite q e f = runReader e $ runReader q $ runTestQueries f

createTableA = "create table A (a1 text, a2 text)"

createTableB = "create table B (b1 text, b2 int)"

selectA = "SELECT * from A"

selectB = "SELECT * from B"

program :: forall r. (Members '[State Int, Sqlite] r) => Sem r ()
program = do
  v <- length @_ @(Text, Text) <$> queryS selectA ()
  modify $ (+) v
  v <- length @_ @(Text, Int) <$> queryS selectB ()
  modify $ (+) v

queryMockup :: SqliteQueryMockup
queryMockup = SqliteQueryMockup $ \case
  selectA -> const $ read <$> ["paolo, blue", "enrico, red"]
